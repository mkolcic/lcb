/*---------- gulpfile.js ------------*/
var gulp = require('gulp'),
    less = require('gulp-less'),
    clean = require('gulp-clean');
    sourcemaps = require('gulp-sourcemaps');
    concat = require('gulp-concat');
    gutil = require('gulp-util');

gulp.task('less', function () {
    return gulp.src(['src/lcb/Bundle/WebBundle/Resources/public/less/*.less'])
        .pipe(less({compress: true}))
        .pipe(gulp.dest('web/assets/css/'));
});

gulp.task('images', function () {
    return gulp.src([
        'src/lcb/Bundle/WebBundle/Resources/public/images/*'
    ])
        .pipe(gulp.dest('web/assets/images/'))
});
gulp.task('fonts', function () {
    return gulp.src(['src/lcb/Bundle/WebBundle/Resources/public/fonts/*'])
        .pipe(gulp.dest('web/assets/fonts/'))
});
gulp.task('build-js', function () {
    return gulp.src(
                    ['web/assets/vendor/jquery/dist/jquery.min.js',
                     'web/assets/vendor/bootstrap/dist/js/bootstrap.min.js']
                     )
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        //only uglify if gulp is ran with '--type production'
        .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('web/assets/js/'));
});
gulp.task('clean', function () {
    return gulp.src(
        ['  web/assets/css/*',
            'web/assets/js/*',
            'web/assets/images/*',
            'web/assets/fonts/*',
            'src/lcb/Bundle/WebBundle/Resources/public/js/*'
        ])
        .pipe(clean());
});
gulp.task('default', ['clean'], function () {
    var tasks = ['images', 'fonts', 'less', 'build-js'];
    tasks.forEach(function (val) {
        gulp.start(val);
    });
});
gulp.task('watch', function () {
    var less = gulp.watch('src/lcb/Bundle/WebBundle/Resources/public/less/*.less', ['less']);
});