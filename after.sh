cd lcb 

echo "#### INFO: gulp modules install"
npm install gulp-less
npm install gulp-clean
npm install gulp-sourcemaps
npm install gulp-concat


echo "##### INFO: Updating apt cache"
sudo apt-get update

echo "##### INFO: Executing composer install"
composer install

echo "##### INFO: Executing bower install"
bower install --allow-root

echo "##### INFO: Executing npm install"
npm install

echo "##### INFO: Executing gulp less"
gulp less

echo "##### INFO: Executing project init scripts"
php bin/console doctrine:schema:update --force
