<?php

namespace lcb\Bundle\CategoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('lcbCategoryBundle:Default:index.html.twig');
    }
}
