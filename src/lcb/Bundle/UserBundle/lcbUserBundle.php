<?php

namespace lcb\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class lcbUserBundle extends Bundle
{
  public function getParent()
  {
    return 'FOSUserBundle';
  }
}
