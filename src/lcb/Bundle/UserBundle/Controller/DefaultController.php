<?php

namespace lcb\Bundle\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('lcbUserBundle:Default:index.html.twig');
    }
}
