<?php

namespace lcb\Bundle\CommentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('lcbCommentBundle:Default:index.html.twig');
    }
}
