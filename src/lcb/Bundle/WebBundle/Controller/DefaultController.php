<?php

namespace lcb\Bundle\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('lcbWebBundle:Frontend/Home:index.html.twig');
    }
}
