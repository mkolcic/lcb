<?php

namespace lcb\Bundle\ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('lcbArticleBundle:Default:index.html.twig');
    }
}
