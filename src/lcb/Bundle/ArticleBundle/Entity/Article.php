<?php

namespace lcb\Bundle\ArticleBundle\Entity;

/**
 * Article
 */
class Article
{

    private $category;
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $text;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set text
     *
     * @param string $text
     *
     * @return Article
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return MainTable
     */
    public function setCategory($category)
    {
      $this->category = $category;

      return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
      return $this->category;
    }

    public function __toString()
    {
        return $this->title;
    }
}

